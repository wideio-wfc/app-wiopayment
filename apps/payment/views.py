# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response, RequestContext, redirect

import math
import stripe

from wioframework import utilviews as uv
from payment import models
import  settings
from functools import reduce

VIEWS = reduce(lambda x, y: x + uv.AUDLV(y), models.MODELS, [])

# VIEWS = ( [ ]
#   + uv.AUDLV(models.Payment, **dict(dec.for_admin_only))
#   + uv.AUDLV(models.CreditAccount, **dict(dec.for_admin_only))
#   + uv.AUDLV(models.Transaction, **dict(dec.for_admin_only))
# )


def credits_for_amount(n):
    #  n=n/100.0
    b = (n / 10000)
    if (b > 1):
        b = 1
    return int(math.floor((b) * (n * 2500) + (1 - b) * (n * 500)))

# def credits_for_amount_view(request):
#  return


def credits_page(request):
    if not request.user.is_authenticated():
        return redirect('/')
    if request.method == 'GET':
        return render_to_response('payment/credits.html', {
            'request': request,
        }, context_instance=RequestContext(request))

    elif request.method == 'POST':
        p = models.Payment(
            user=request.user,
            amount=int(
                float(
                    request.POST["amount"]) *
                100),
            credit=credits_for_amount(
                float(
                    request.POST["amount"])))
        p.save()
        return render_to_response('payment/buy_credits.html', {
            'request': request,
            'amount': p.get_amount(),
            'credit': p.credit,
            'id_payment': p.id,
        }, context_instance=RequestContext(request))

credits_page.default_url = "credits"
credits_page.SSL = True
VIEWS.append(credits_page)


def buycredits(request):
    if not request.user.is_authenticated():
        return redirect('/')
    # if request.method == 'GET':
    #    return render_to_response('payment/credits.html', {
    #        'base': base,
    #        'request': request,
    #        }, context_instance=RequestContext(request))

    elif request.method == 'POST':
        error = ""
        if 'id_payment' in request.POST:
            p = models.Payment.objects.get(id=request.POST['id_payment'])
            error = p.handle_payment(request)
            if error:
                return render_to_response('payment/buy_credits.html', {
                    'request': request,
                    'error': error,
                    'id_payment': p.id,
                }, context_instance=RequestContext(request))
            else:
                return render_to_response('payment/credits.html', {
                    'request': request,
                    'message': True,
                }, context_instance=RequestContext(request))

buycredits.default_url = "buycredits"
buycredits.SSL = True
VIEWS.append(buycredits)


def stripe_customer_register(request):
    if not request.user.is_authenticated():
        return redirect('/')

    stripe.api_key = settings.STRIPE_SECRET_KEY
    token = request.POST['stripeToken']

    try:
        customer = stripe.Customer.create(
            card=token,
            description=request.user.email
        )
        try:
            request.user.stripecustomer.customer_id = customer.id
            request.user.stripecustomer.save()
        except ObjectDoesNotExist as e:
            stripe_customer = models.StripeCustomer(
                user=request.user,
                customer_id=customer.id)
            stripe_customer.save()
    except stripe.CardError as e:
        print e

    return render_to_response('payment/credits.html', {
        'request': request,
    }, context_instance=RequestContext(request))
stripe_customer_register.default_url = "stripe_register"
stripe_customer_register.SSL = True
VIEWS.append(stripe_customer_register)


def history(request):
    user = request.user
    if not user.is_authenticated():
        return redirect('/')
    #~ internal_payments = InternalPayment.objects.filter(user_id=user.id).order_by('-created_at')
    payments = models.Payment.objects.filter(
        user_id=user.id).order_by('-created_at')
    if ("AJAX" in request.REQUEST and request.REQUEST["AJAX"]):
        #~ internal_payments = internal_payments[:5]
        payments = payments[:5]
    return render_to_response('payment/history.html',
                              {
                                  'user': request.user,
                                  #       'internal_payments': internal_payments,
                                  'payments': payments,
                              }, context_instance=RequestContext(request))
history.default_url = "history"
VIEWS.append(history)
