import random
import time

from django.db import models

from wioframework.amodels import wideiomodel, wideio_timestamped, wideio_timestamped_autoexpiring, wideio_owned, get_dependent_models, wideio_action
from wioframework.atomic_ops import atomic_incr

def get_wideio_account():
    try:
        return CreditAccount.objects.get(description="***WIDEIO ACCOUNT***")
    except:
        c = CreditAccount()
        c.description = "***WIDEIO ACCOUNT***"
        c.save()
        return CreditAccount.objects.get(description="***WIDEIO ACCOUNT***")


@wideio_timestamped
@wideiomodel
class Transaction(models.Model):

    """
    As much as possible we must try to be ACID in the transaction we achieve..
    Non-rel db are not great for that but we'll do our best for that.
    """
    ctrname_s = "transaction"
    ctrname_p = "transactions"
    from_account = models.ForeignKey( 'payment.CreditAccount', related_name="commited_debits", null=True)  # NULL means of wide io
    to_account = models.ForeignKey( 'payment.CreditAccount', related_name="commited_credits", null=True)  # NULL means of wide io

    description = models.TextField(max_length=255)
    amount = models.DecimalField(decimal_places=2, max_digits=12)

    @staticmethod
    def can_add(request):
        if (request.user.is_authenticated):
            return True

    def can_update(self, request):
        return False

    def can_delete(self, request):
        return request.user.is_superuser

    def Lock(self):
        # BEWARE OF DEADLOCKS !
        l1 = True
        while not l1:
            if self.from_account:
                l1 = self.from_account.try_acquire_lock()
            else:
                l1 = get_wideio_account().try_acquire_lock()
            if l1:
                if self.to_account:
                    l1 = l1 and self.to_account.try_acquire_lock()
                else:
                    l1 = l1 and get_wideio_account().try_acquire_lock()
                if not l1:
                    if self.from_account:
                        l1 = self.from_account.release_lock()
                    else:
                        l1 = get_wideio_account().release_lock()
                    time.sleep(random.random() * .01)

    def Unlock(self):
        if self.from_account:
            self.from_account.release_lock()
        else:
            get_wideio_account().release_lock()
        if self.to_account:
            self.to_account.release_lock()
        else:
            get_wideio_account().release_lock()

    # TODO: Verify if lock is useful and if transaction can be reverted
    def on_add(self, request):
        self.Lock()
        ##
        # OPERATE TRANSACTION
        ##
        try:
            if self.from_account:
                self.from_account = atomic_incr(
                    self.from_account, 'credit', -self.amount)
                buyer = self.from_account.owner.username
            else:
                atomic_incr(get_wideio_account(), 'credit', -self.amount)
                buyer = "WIDE IO"
            if self.to_account:
                self.to_account = atomic_incr(
                    self.to_account,
                    'credit',
                    self.amount)
                seller = self.to_account.owner.username
            else:
                atomic_incr(get_wideio_account(), 'credit', self.amount)
                seller = "WIDE IO"
            print "Transaction success:", self.amount, "credits went from", buyer, "to", seller, "for", self.description
        except:
            print "Failure: Transaction aborted, no credit movement operated."
        self.Unlock()

    def on_delete(self, request):
        self.Lock()
        ##
        # OPERATE TRANSACTION
        ##
        saved_credit = self.from_account.credit if self.from_account is not None else get_wideio_account(
        ).credit
        try:
            if self.from_user:
                self.from_account = atomic_incr(
                    self.from_account,
                    'credit',
                    self.amount)
            else:
                atomic_incr(get_wideio_account(), 'credit', self.amount)
            if self.to_account:
                self.to_account = atomic_incr(
                    self.to_account.account, 'credit', -self.amount)
            else:
                atomic_incr(get_wideio_account(), 'credit', -self.amount)
        except:  # not safe, should precise the exception.
            self.from_account.credit = saved_credit
        self.Unlock()
