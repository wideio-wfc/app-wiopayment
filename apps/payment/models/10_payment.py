import datetime

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from wioframework.amodels import wideiomodel, wideio_timestamped, wideio_timestamped_autoexpiring, wideio_owned, get_dependent_models, wideio_action

@wideio_timestamped_autoexpiring(
    expire=datetime.timedelta(2))  # Expires in 2 days
@wideiomodel
class Payment(models.Model):
    PENDING = "pending"
    DONE = "done"
    ctrname_s = "payment"
    ctrname_p = "payments"
    user = models.ForeignKey('accounts.UserAccount')

    uuid = models.CharField(max_length=64)
    amount = models.IntegerField()
    payment_type = models.CharField(max_length=64, default="stripe")
    credit = models.IntegerField()
    transaction_state = models.CharField(max_length=16, default="pending", choices=(("pending", "pending"), ("done", "done")))

    def handle_payment(self, request):
        if self.transaction_state == Payment.DONE:
            return "Payment is already done."
        stripe_customer = None
        try:
            stripe_customer = self.user.stripecustomer
        # TODO: handle lack of stripecustomer id
        except ObjectDoesNotExist as e:
            return "No StripeCustomer found."

        try:
            stripe.api_key = STRIPE_SECRET_KEY
            stripe.Charge.create(
                amount=self.amount,
                currency=STRIPE_CURRENCY,
                customer=stripe_customer.customer_id)
        except stripe.CardError as e:
            # print e
            from backoffice import lib as debug
            debug.log_error(e)
            raise e
        else:
            account = self.user.get_account()
            try:
                while account.try_acquire_lock() is False:
                    pass
                account.credit += self.credit
                account.save()
                self.transaction_state = Payment.DONE
                self.fix()  # If the payment is complete, it will not expire.
                self.save()
            finally:
                account.release_lock()

    def get_amount(self):
        return str(self.amount / 100.) + '&pound;'

    def __unicode__(self):
        return self.user.email + " " + \
            str(self.amount) + " " + self.transaction_state
