import random

from django.db import models

from wioframework.amodels import wideiomodel, wideio_timestamped, wideio_timestamped_autoexpiring, wideio_owned, get_dependent_models, wideio_action


@wideio_timestamped
@wideio_owned("owner")
@wideiomodel
class CreditAccount(models.Model):

    """
    As much as possible we must try to be ACID in the transaction we achieve..
    Non-rel db are not great that for but we'll do our best for that.
    """
    ctrname_s = "account"
    ctrname_p = "accounts"
    description = models.TextField(max_length=255)
    credit = models.IntegerField(default=0)
    auto_topup = models.BooleanField(default=False)
    lock = models.CharField(max_length=256, null=True, blank=True)

    @staticmethod
    def can_add(request):
        return request.user.is_superuser

    def clean(self):
        if (self.owner.is_superuser):
            a = self.objects.filter(description="***WIDEIO ACCOUNT***")
            if (a.count() == 0):
                return True
            if (a[0] == self):
                return True
        if (self.description == "***WIDEIO ACCOUNT***"):
            from django.core.exceptions import ValidationError
            raise ValidationError('Reserved account.')

    def can_update(self, request):
        if (self.description == "***WIDEIO ACCOUNT***"):
            return False
        return request.user.is_superuser

    def can_delete(self, request):
        return False

    def try_acquire_lock(self):
        """
        THIS IS NOT 100% SAFE | TODO CHECK IF BETTER PRIMITIVE ARE AVAILABLE

        F() ?

        USE OF PYMONGO NATIVE API ?? docs.mongodb.org/manual/tutorial/perform-two-phase-commits/
        """
        if (self.lock is not None and len(self.lock)):
            return False
        else:
            r = str(random.random())
            self.lock = r
            self.save()
            return CreditAccount.objects.get(id=self.id).lock == r

    def release_lock(self):
        self.lock = None
        self.save()

    class WIDEIO_Meta:
        icon = "ion-social-usd"
        CAN_TRANSFER=False

        class Actions:

            @wideio_action(
                possible=lambda o, r: (
                    (r.user == o.owner) and (
                        o.credit < 500)))
            def add_100_credits(self, request):
                t = Transaction()
                t.from_account = None
                t.to_account = self
                t.description = "Credits for demo usage"
                t.amount = 100
                t.save()
                t.on_add(request)
                updated = self.__class__.objects.get(id=self.id)
                return "alert('Done. Your new balance is " + \
                    str(updated.credit) + "' );"

            @wideio_action(
                possible=lambda o, r: (
                    (r.user == o.owner) and (
                        o.auto_topup == False)))
            def enable_autotopup(self, request):
                self.auto_topup = True
                self.save()
                return "alert('Done');"

            @wideio_action(
                possible=lambda o, r: (
                    (r.user == o.owner) and (
                        o.auto_topup)))
            def disable_autotopup(self, request):
                self.auto_topup = False
                self.save()
                return "alert('Done');"
