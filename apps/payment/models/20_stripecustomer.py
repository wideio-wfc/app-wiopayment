
from wioframework.amodels import *
from settings import STRIPE_SECRET_KEY, STRIPE_CURRENCY
import stripe

@wideio_timestamped
@wideiomodel
class StripeCustomer(models.Model):
    # FIXME security permission
    #user = models.OneToOneField(UserAccount, null=False)
    user = models.OneToOneField('accounts.UserAccount', null=False)
    customer_id = models.CharField(max_length=100)
    activated=models.BooleanField(default=True)
    def __unicode__(self):
        return self.customer_id
