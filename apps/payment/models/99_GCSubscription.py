import datetime

from django.db import models

from wioframework.amodels import wideiomodel, wideio_timestamped, wideio_timestamped_autoexpiring, wideio_owned, get_dependent_models, wideio_action

@wideio_owned(personal=2)
@wideiomodel
class GCSubscription(models.Model):
    amount = models.IntegerField()
    description=models.TextField()
    deposit_name=models.CharField(max_length=16, default="WIDE IO", blank=True)
    preauth_id=models.CharField(max_length=32, default="WIDE IO",blank=True)
    setup_fee=models.IntegerField(default=0)

    def on_add(self,request):
       import gocardless
       self.setup_fee=0
       self.save()
       url=gocardless.client.new_subscription_url(self.amount, 1, "month", name=self.name+"#"+self.id,description="", expires_at=(datetime.datetime.now()+datetime.timedelta(30*3)),state=self.id,redirect_uri=self.get_view_url(), cancel_url=self.get_view_url(),setup_fee=self.setup_fee)
       return {'_redirect':url}

    def can_update(self,request):
        return False

    def on_view(self,request):
        ### IT MAY BE THAT WE ARE ACTUALLY ON A GO CARDLESS CALL BACK HANDLE THIS
        #resource_id=0WQ2ZRHBC7&resource_type=pre_authorization
        #&resource_uri=https%3A%2F%2Fsandbox.gocardless.com%2Fapi%2Fv1%2Fpre_authorizations%2F0WQ2ZRHBC7
        #&signature=bce3de4b3efdec84547f7eee215c306aedde95667ba36852e8cde2b3a387bfb5
        #&state=94b6474e-f700-11e4-aaa5-bc764e0865ba
        if "resource_id" in request.GET:
          gocardless.cient.confirm_resource(request.GET.__dict__)
          self.preauth_id=request.GET("resource_id")
          self.save()

    class WIDEIO_Meta:
        NO_DRAFTS=True
        CAN_TRANSFER=False
        form_exclude=['preauth_id']
        class Actions:
            @wideio_action(possible=lambda s,r:r.user.is_staff)
            def cash_in():
                import gocardless
                pre_auth = gocardless.client.pre_authorization(self.preauth_id)
                pre_auth.create_bill(self.amount, name=self.deposit_name,
                 charge_customer_at=datetime.datetime.now().strftime("%Y-%m-%%d"))


