from django.db import models

from wioframework.amodels import wideiomodel, wideio_timestamped, wideio_timestamped_autoexpiring, wideio_owned, get_dependent_models, wideio_action


@wideiomodel
class BuyableAnalytics(models.Model):
    times_bought = models.IntegerField()
    amount_bought = models.DecimalField(decimal_places=2, max_digits=15)
    # dates_history = ListField(models.DateTimeField)
    # times_bought_history = ListField(models.IntegerField())
    # amount_bought_history = ListField(models.DecimalField(decimal_places=2, max_digits=15))
