#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import datetime

from wioframework.amodels import *
from wioframework import decorators as dec
from extfields import fields as ef


@wideio_owned()
@wideio_timestamped_autoexpiring(expire=datetime.timedelta(days=2), delete_database_entry_on_expire=False)
@wideiomodel
class QuoteRequest(models.Model):

    """
    A request by user to provide a specific price for a specific volume
    """
    algorithm = models.ForeignKey(
        'science.Algorithm',
        blank=True,
        null=True,
        related_name="related_quotes",
        help_text="A link to the algorithm that you want to run")

    number_of_queries = models.IntegerField()

    input_types = ef.JSONField( null=True, blank=True,
        help_text="Inputs of the algorithm component in this request")
    parameters = ef.JSONField( null=True, blank=True,
        help_text="Parameters that are used to configure the algorithm component for one or many request")

    use_webhook = models.BooleanField(default=True)

    #batch_query = models.BooleanField(default=False)
    #dataset_expression = models.TextField(null = True, blank = True, help_text = "Dataset expression that is of main interest")

    def on_add(self, request):
        from payment.models import Quote
        q = Quote()
        q.request = self
        q.cost = 100
        q.save()
        return {"_redirect": q.get_view_url()}

    class WIDEIO_Meta:
        NO_DRAFT = True
        icon = "icon-flag-alt"
        form_exclude = [
            'job_id',
            'ordered_by',
            'status',
            'access_key',
            'token',
            'result',
            'use_server'
        ]
        permissions = dec.perm_for_logged_users_only
