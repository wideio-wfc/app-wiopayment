# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
###
import math

cost_of_one_compute_cloud_host_gbp_per_month = 40
cost_of_one_database_gbp_per_month = 60
cost_of_one_storage_gbp_per_month = 60
cost_of_one_web_gbp_per_month = 30

cost_of_one_active_user_gbp_per_month = 0.2 * cost_of_one_web_gbp_per_month + 0.1 * cost_of_one_database_gbp_per_month + \
    0.1 * cost_of_one_database_gbp_per_month + 0.25 * \
    cost_of_one_compute_cloud_host_gbp_per_month
profit_factor = 8
official_of_one_active_user_gbp_per_month = cost_of_one_active_user_gbp_per_month * \
    profit_factor
official_of_one_active_user_gbp_per_second = (
    official_of_one_active_user_gbp_per_month * 12 / 365.25) / (24 * 3600)

one_credit_cost = official_of_one_active_user_gbp_per_second
s1 = 10**(math.ceil(math.log10(1. / one_credit_cost)))
s14 = s1 / 4
if (s14 > (1. / one_credit_cost)):
    s1 = 10**(math.floor(math.log10(1. / one_credit_cost)))
    s14 = s1 / 4
s2 = float(math.floor((1. / one_credit_cost) / s14) * s14)
# print s14,s2
credits_per_gbp = int(s2)
# print credits_per_gbp
one_credit_cost = 1. / credits_per_gbp


# print credits_per_gbp, one_credit_cost
